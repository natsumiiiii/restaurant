<?php get_header(); ?>

<div class="phot_page">
    <h2>PhotoGallery</h2>

    <div class="pg_wrap">
        <a class="pg" href="<?php echo home_url(); ?>/course/">
          <p>Course</p>
          <img src="<?php echo get_template_directory_uri();?>/img/course03.jpg" alt="">
        </a>
        <a class="pg" href="<?php echo home_url(); ?>/food/">
          <p>Food</p>
          <img src="<?php echo get_template_directory_uri();?>/img/food07.jpg" alt="">
        </a>
        <a class="pg" href="<?php echo home_url(); ?>/sweets/">
          <p>Sweets</p>
          <img src="<?php echo get_template_directory_uri();?>/img/phot2.jpg" alt="">
        </a>
        <a class="pg" href="<?php echo home_url(); ?>/appearance/">
          <p>Appearance</p>
          <img src="<?php echo get_template_directory_uri();?>/img/Appearance03.jpg" alt="">
        </a>
    </div>
</div>


<?php get_footer(); ?>
