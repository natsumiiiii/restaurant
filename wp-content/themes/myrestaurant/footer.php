<div id="footer">
   <div id="footMenu">
     <div class="left_footer">
         <ul class="footer_list">
             <li class="m_menu">
               <p><a href="<?php echo home_url(); ?>/top/">Top</a></p>
             </li>
             <li class="m_menu">
               <p>Menu</p>
               <ul class="s_menu">
                 <li><a href="<?php echo home_url(); ?>/course/">Course</a></li>
                 <li><a href="<?php echo home_url(); ?>/food/">Food</a></li>
                 <li><a href="<?php echo home_url(); ?>/drink/">Drink</a></li>
               </ul>
             </li>
             <li class="m_menu">
               <p>Info</p>
               <ul class="s_menu">
                 <li><a href="<?php echo home_url(); ?>/good/">Menbers</a></li>
                 <li><a href="<?php echo home_url(); ?>/commitment/">Commitment</a></li>
                 <li><a href="<?php echo home_url(); ?>/appearance/">Appearance</a></li>
               </ul>
             </li>
             <li class="m_menu">
               <p>Contact</p>
               <ul class="s_menu">
                 <li><a href="<?php echo home_url(); ?>/mail/">Mail</a></li>
                 <li><a href="<?php echo home_url(); ?>/reservation/">Reservation</a></li>
                 <li><a href="<?php echo home_url(); ?>/questions/">Questions</a></li>
               </ul>
             </li>
         </ul>
     <div class="tel">
       <p>迷ったら　<br class="spbr">まずはお気軽にお電話ください</p>
       <a class="tel_no" href="tel:000-1234-5678">TEL 0120-000-000</a>
       <p>※受付時間 10:00~18:00　<br class="spbr">毎週木曜定休日</p>
     </div>
  </div>
  <div class="google_map">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3249.993378825318!2d139.62919721452147!3d35.45495834995712!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x60185c5d7366a111%3A0x3661ef0a3534c8c7!2z5qiq5rWc44Op44Oz44OJ44Oe44O844Kv44K_44Ov44O8!5e0!3m2!1sja!2sjp!4v1585313155781!5m2!1sja!2sjp" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
  </div>

   </div><!-- /#footerMenu -->
   <div id="copy">Copyright &copy; 2020 Natsumiiiii All Rights Reserved.</div>
</div><!-- /#footer -->
</div><!-- /#top -->
<?php wp_footer();?>
</body>
</html>
