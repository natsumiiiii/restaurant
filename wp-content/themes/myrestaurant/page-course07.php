<?php get_header(); ?>

<div class="coursepage">

  <h2>【ホワイトデー】<br>乾杯ロゼスパークリング付！<br>魚＆肉の Wメインと<br>デザートなど全6品</h2>
  <h3>〜メニュー〜</h3>
  <p>【飲物】&nbsp;</p>
  <p>・ロゼスパークリングワイン　1杯&nbsp;</p>
  <p>（ノンアルコールご希望の方は、ソフトドリンクに変更いたします）&nbsp;</p>
  <p>【料理】&nbsp;</p>
  <p>アミューズ / 前菜 / スープ / 魚料理 / 肉料理 / デザート / コーヒー&nbsp;</p>
  <br>
  <h3>～メニュー例～&nbsp;</h3>
  <p>・アミューズ&nbsp;</p>
  <p>・ノルウェーサーモンのコンフィ　春菊のサラダ添え　塩昆布のアクセント&nbsp;</p>
  <p>・季節のスープ&nbsp;</p>
  <p>・横浜中央卸売市場から届く鮮魚のクルート焼きに　タイムのソースを合わせて&nbsp;</p>
  <p>・はかた地どりの胸肉ともも肉のロティ　ソース　アルビュフェラ&nbsp;</p>
  <p>・ホワイトデー特製デザート&nbsp;</p>
  <p>・コーヒー</p>
  <br>
  <p>※メニューは仕入状況等により変更となる場合がございます。予めご了承ください。</p>

</div>
<div class="backimg">
  <img src="<?php echo get_template_directory_uri();?>/img/course01.jpg" alt="">
</div>

<?php get_footer(); ?>
