<?php get_header(); ?>

<div class="coursepage ">
  <h2>【本格フレンチをご堪能】<br>特製アペリティフ<br>＆厳選ワインの計2杯付き！<br>横浜ベイの夜景と全4品をご堪能</h2>
  <h3>〜メニュー〜</h3>
  <p>■□お飲み物□■&nbsp;</p>
  <p>・特製アペリティフ　1人1杯&nbsp;</p>
  <p>・ワイン（赤 or 白）　1人1杯&nbsp;</p>
  <p>計1人2杯付き（ノンアルコール対応可能です）&nbsp;</p>
  <br>
  <p>■□お料理□■&nbsp;</p>
  <p>＜ある日のメニュー例＞&nbsp;</p>
  <p>【アミューズ】&nbsp;</p>
  <p>【パン】&nbsp;</p>
  <p>【前菜】&nbsp;</p>
  <p>【スープ】&nbsp;</p>
  <p>【魚料理】&nbsp;</p>
  <p>【デザート】&nbsp;</p>
  <p>【カフェ】&nbsp;</p>
  <p>コーヒー または 紅茶&nbsp;</p>
  <p>【アニバーサリー特典】</p>
  <p>特別なお祝いにデザートにメッセージ入れをサービス(通年)</p>
  <br>
  <p>※メニューは仕入状況等により変更となる場合がございます。</p>
  <p>予めご了承ください。</p>

</div>
<div class="backimg">
  <img src="<?php echo get_template_directory_uri();?>/img/course01.jpg" alt="">
</div>


<?php get_footer(); ?>
