$(function(){
	// $('#main>img').css({
	// 	opacity:0
	// });
	$('#main>img').css({
		opacity:1,
		transition:7000 + 'ms'
	});
	setTimeout(function(){
		$('.topheader').css({
			top:0,
			transition:1000 + 'ms'
		});
	},3000);
});





//したからふわっと
$(function(){
    $(window).scroll(function (){
        $('.fadein').each(function(){
            var targetElement = $(this).offset().top;
            var scroll = $(window).scrollTop();
            var windowHeight = $(window).height();
            if (scroll > targetElement - windowHeight + 200){
                $(this).css('opacity','1');
                $(this).css('transform','translateY(0)');
            }
        });
    });
});


//横スライドショー
$(function (){
	$('.slider').slick({
	    autoplay:true,
	    autoplaySpeed:0,
			cssEase:'linear',
			speed:5000,
	    dots:true,
	    slidesToShow:5,
	    slidesToScroll:1,
			responsive:[
				{
					breakpoint:480,
					settings: {
						slidesToShow:2,
					}
				}
			]
	});
});

$(document).ready(function(){
　$("p.question").on("click", function() {
　　$(this).next().slideToggle(200);
　});
});


// レスポンシブ
$(function(){
    $(".btn-gnavi").on("click", function(){
        // ハンバーガーメニューの位置を設定
        var rightVal = 0;
        if($(this).hasClass("open")) {
            // 位置を移動させメニューを開いた状態にする
            rightVal = -300;
            // メニューを開いたら次回クリック時は閉じた状態になるよう設定
            $(this).removeClass("open");
        } else {
            // メニューを開いたら次回クリック時は閉じた状態になるよう設定
            $(this).addClass("open");
        }

        $(".nav_menu").stop().animate({
            right: rightVal
        }, 200);
    });
});
