<?php get_header(); ?>

<div class="reservation_page">
<?php
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();
?>


      <h2><?php the_title();?></h2>
      <?php the_content();?>


<?php
	} // end while
} // end if
?>
</div>


<?php get_footer(); ?>
