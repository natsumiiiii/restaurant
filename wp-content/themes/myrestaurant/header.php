<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="imagetoolbar" content="no" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta name="format-detection" content="telephone=no">
<meta name="viewport" content="width=device-width,instal-scale=1.0">
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/style.css" type="text/css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/reset.css"　type="text/css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/mytheme.css"　type="text/css">
<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/jquery.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/javascript.js"></script>
<!-- slick -->
<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/slick/slick.min.js"></script>
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/slick/slick-theme.css"　type="text/css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/slick/slick.css"　type="text/css">

<?php wp_head();?>
</head>
<body<?php body_class();?>>
<div id="top">
   <div class="headWrap">
      <div id="header">
         <h1><a href="<?php echo home_url(); ?>/top/"><span>R</span>ESTAURANT</a></h1>
         <//?php if ( wp_is_mobile() ) : ?>
        <div id="nav-drawer">
          <input id="nav-input" type="checkbox" class="nav-unshown">
          <label id="nav-open" for="nav-input"><span></span></label>
          <label class="nav-unshown" id="nav-close" for="nav-input"></label>
          <div id="nav-content">
            <ul class="sp_only">
                <li class="menu__single">
                  <p><a class="menu__single-top" href="<?php echo home_url(); ?>/top/">Top</a></p>
                </li>
                <li class="menu__single init-bottom">
                  <p>Menu</p>
                  <ul class="menu__second-level">
                    <li><a href="<?php echo home_url(); ?>/course/">Course</a></li>
                    <li><a href="<?php echo home_url(); ?>/food/">Food</a></li>
                    <li><a href="<?php echo home_url(); ?>/drink/">Drink</a></li>
                  </ul>
                </li>
                <li class="menu__single init-bottom">
                  <p>Information</p>
                  <ul class="menu__second-level">
                    <li><a href="<?php echo home_url(); ?>/members/">Menbers</a></li>
                    <li><a href="<?php echo home_url(); ?>/commitment/">Commitment</a></li>
                    <li><a href="<?php echo home_url(); ?>/appearance/">Appearance</a></li>
                  </ul>
                </li>
                <li class="menu__single init-bottom">
                  <p>Contact</p>
                  <ul class="menu__second-level">
                    <li><a href="<?php echo home_url(); ?>/mail/">Mail</a></li>
                    <li><a href="<?php echo home_url(); ?>/reservation/">Reservation</a></li>
                    <li><a href="<?php echo home_url(); ?>/questions/">Questions</a></li>
                  </ul>
                </li>
            </ul>

          </div>
        </div>
      <//?php endif; ?>

      </div><!-- /#header -->
   <div id="menu">
      <ul class="pc_only">
          <li class="menu__single">
            <p><a href="<?php echo home_url(); ?>/top/">Top</a></p>
          </li>
          <li class="menu__single init-bottom">
            <p>Menu</p>
            <ul class="menu__second-level">
              <li><a href="<?php echo home_url(); ?>/course/">Course</a></li>
              <li><a href="<?php echo home_url(); ?>/food/">Food</a></li>
              <li><a href="<?php echo home_url(); ?>/drink/">Drink</a></li>
            </ul>
          </li>
          <li class="menu__single init-bottom">
            <p>Information</p>
            <ul class="menu__second-level">
              <li><a href="<?php echo home_url(); ?>/good/">Menbers</a></li>
              <li><a href="<?php echo home_url(); ?>/commitment/">Commitment</a></li>
              <li><a href="<?php echo home_url(); ?>/appearance/">Appearance</a></li>
            </ul>
          </li>
          <li class="menu__single init-bottom">
            <p>Contact</p>
            <ul class="menu__second-level">
              <li><a href="<?php echo home_url(); ?>/mail/">Mail</a></li>
              <li><a href="<?php echo home_url(); ?>/reservation/">Reservation</a></li>
              <li><a href="<?php echo home_url(); ?>/questions/">Questions</a></li>
            </ul>
          </li>
      </ul>
   </div><!-- /#menu -->
  </div><!-- /#headWrap -->
