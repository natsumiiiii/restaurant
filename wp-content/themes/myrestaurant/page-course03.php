<?php get_header(); ?>

<div class="coursepage">

  <h2>【平日限定】横浜駅直結！食前酒付<br>季節のスープから始まる<br>地元食材で仕立てたコース！<br>本日のデザート付</h2>
  <h3>〜メニュー〜</h3>
  <p>■食前酒1杯（オリジナルカクテル）&nbsp;</p>
  <p>【ある日のメニュー例】&nbsp;</p>
  <p>■季節野菜のスープ&nbsp;</p>
  <p>■メインディッシュ&nbsp;<br>　本日のお魚料理をご用意いたします&nbsp;</p>
  <p>■デザート&nbsp;<br>　本日のおすすめデザートをご用意いたします&nbsp;</p>
  <p>■コーヒーまたは紅茶</p>
  <br>
  <p>※メニューは仕入状況等により変更となる場合がございます。予めご了承ください。</p>

</div>

<div class="backimg">
  <img src="<?php echo get_template_directory_uri();?>/img/course01.jpg" alt="">
</div>

<?php get_footer(); ?>
