<?php get_header(); ?>

<div class="coursepage">

  <h2>【最上階の美しい景色と共に】<br>窓側確約＆限定1組！<br>乾杯スパークリング付<br>月ごとに旬をお届けWメイン等全5品</h2>
  <h3>〜メニュー〜</h3>
  <p>■□お飲み物□■&nbsp;</p>
  <p>乾杯にスパークリングワインをご用意致します&nbsp;</p>
  <p>■□お料理□■&nbsp;</p>
  <p>・前菜&nbsp;</p>
  <p>・季節野菜のスープ&nbsp;</p>
  <p>・魚料理&nbsp;</p>
  <p>・肉料理&nbsp;</p>
  <p>・デザート盛り合わせ&nbsp;</p>
  <p>・コーヒーまたは紅茶&nbsp;</p>
  <br>
  <p>※特典：記念日にてご利用の方には、メッセージプレートを添えてアレンジいたします。※メニューは仕入状況等により変更となる場合がございます。予めご了承ください。</p>
</div>

<div class="backimg">
  <img src="<?php echo get_template_directory_uri();?>/img/course01.jpg" alt="">
</div>

<?php get_footer(); ?>
