<?php get_header(); ?>

<div class="gaikanpage">

<h2>外観・室内<br class="spbr">フォトギャラリー</h2>

<div class="gaikanwrap">
  <div class="gaikan">
      <img src="<?php echo get_template_directory_uri();?>/img/Appearance01.jpg" alt="">
      <h4></h4>
  </div>
  <div class="gaikan">
      <img src="<?php echo get_template_directory_uri();?>/img/Appearance02.jpg" alt="">
      <h4></h4>
  </div>
  <div class="gaikan">
      <img src="<?php echo get_template_directory_uri();?>/img/Appearance03.jpg" alt="">
      <h4></h4>
  </div>
  <div class="gaikan">
      <img src="<?php echo get_template_directory_uri();?>/img/Appearance04.jpg" alt="">
      <h4></h4>
  </div>
  <div class="gaikan">
      <img src="<?php echo get_template_directory_uri();?>/img/Appearance05.jpg" alt="">
      <h4></h4>
  </div>
  <div class="gaikan">
      <img src="<?php echo get_template_directory_uri();?>/img/Appearance06.jpg" alt="">
      <h4></h4>
  </div>
  <div class="gaikan">
      <img src="<?php echo get_template_directory_uri();?>/img/Appearance07.jpg" alt="">
      <h4></h4>
  </div>
  <div class="gaikan">
      <img src="<?php echo get_template_directory_uri();?>/img/Appearance08.jpg" alt="">
      <h4></h4>
  </div>
  <div class="gaikan">
      <img src="<?php echo get_template_directory_uri();?>/img/Appearance09.jpg" alt="">
      <h4></h4>
  </div>
  <div class="gaikan">
      <img src="<?php echo get_template_directory_uri();?>/img/Appearance10.jpg" alt="">
      <h4></h4>
  </div>
</div>


</div>


<?php get_footer(); ?>
