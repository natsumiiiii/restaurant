<?php get_header(); ?>

<div class="coursepage">

  <h2>【ご友人との会食に！】<br>旬食材を使ったコース<br>全4品＆食前酒付き<br>最上階の美景と共に</h2>
  <h3>〜メニュー〜</h3>
  <p>■□お飲物□■&nbsp;</p>
  <p>オリジナルアペリティフ×1杯&nbsp;</p>
  <p>■□お料理□■&nbsp;</p>
  <p>・前菜&nbsp;</p>
  <p>・季節野菜のスープ&nbsp;</p>
  <p>・本日のメインディッシュ&nbsp;</p>
  <p>・デザート盛り合わせ&nbsp;</p>
  <p>・コーヒーまたは紅茶</p>
  <br>
  <p>※メニューは仕入状況等により変更となる場合がございます。予めご了承ください。</p>

</div>

<div class="backimg">
  <img src="<?php echo get_template_directory_uri();?>/img/course01.jpg" alt="">
</div>

<?php get_footer(); ?>
