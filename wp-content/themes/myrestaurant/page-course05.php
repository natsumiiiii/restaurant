<?php get_header(); ?>

<div class="coursepage">

  <h2>【記念日におすすめ！】<br>スペシャルアペリティフ<br>＆ホールケーキ付！<br>魚・肉どちらも堪能できる全6品</h2>
  <h3>〜メニュー〜</h3>
  <p>■□お飲み物□■&nbsp;</p>
  <p>ソムリエ特製スペシャルアペリティフ1杯&nbsp;</p>
  <p>■□お料理□■&nbsp;</p>
  <p>・アミューズ&nbsp;</p>
  <p>・前菜&nbsp;</p>
  <p>・魚料理&nbsp;</p>
  <p>・肉料理&nbsp;</p>
  <p>・デザート盛り合わせ&nbsp;</p>
  <p>・コーヒーまたは紅茶&nbsp;</p>
  <p>・ホールケーキ（15cm）</p>
  <br>
  <p>※メニューは仕入状況等により変更となる場合がございます。予めご了承ください。</p>

</div>

<div class="backimg">
  <img src="<?php echo get_template_directory_uri();?>/img/course01.jpg" alt="">
</div>

<?php get_footer(); ?>
