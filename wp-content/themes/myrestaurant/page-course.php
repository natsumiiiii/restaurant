<?php get_header(); ?>

<div class="coursetoppage">

<h2>コース料理</h2>

    <div class="coursetitle">
      <a href="<?php echo home_url(); ?>/course01">
        <img src="<?php echo get_template_directory_uri();?>/img/course01.jpg" alt="">
        <h3>【お顔合わせプラン・個室確約】<Br>乾杯ロゼシャンパン付！<br>和牛のメインディッシュなど<br>6品フルコース</h3>
      </a>
    </div>
    <div class="coursetitle pppp fadein">
      <a href="<?php echo home_url(); ?>/course02">
        <img src="<?php echo get_template_directory_uri();?>/img/course02.jpg" alt="">
        <h3>【最上階の美しい景色と共に】<br>窓側確約＆限定1組！<br>乾杯スパークリング付<br>月ごとに旬をお届けWメイン等全5品</h3>
      </a>
    </div>
    <div class="coursetitle fadein">
      <a href="<?php echo home_url(); ?>/course03">
        <img src="<?php echo get_template_directory_uri();?>/img/course03.jpg" alt="">
        <h3>【平日限定】横浜駅直結！<br>食前酒付 季節のスープから始まる<br>
          地元食材で仕立てたコース！<br>本日のデザート付</h3>
      </a>
    </div>
    <div class="coursetitle pppp fadein">
      <a href="<?php echo home_url(); ?>/course04">
        <img src="<?php echo get_template_directory_uri();?>/img/course04.jpg" alt="">
        <h3>【ご友人との会食に！】<br>旬食材を使ったコース<br>
          全4品＆食前酒付き<br>最上階の美景と共に</h3>
      </a>
    </div>
    <div class="coursetitle fadein">
      <a href="<?php echo home_url(); ?>/course05">
        <img src="<?php echo get_template_directory_uri();?>/img/course05.jpg" alt="">
        <h3>【記念日におすすめ！】<br>スペシャルアペリティフ<br>＆ホールケーキ付！<br>
          魚・肉どちらも堪能できる全6品</h3>
      </a>
    </div>
    <div class="coursetitle pppp fadein">
      <a href="<?php echo home_url(); ?>/course06">
        <img src="<?php echo get_template_directory_uri();?>/img/course06.jpg" alt="">
        <h3>【シェフセレクション】<br>フレッシュフォアグラのソテーを楽しむ<br>
          和牛メインのコース料理！<br>シェフお薦全6品</h3>
      </a>
    </div>
    <div class="coursetitle fadein">
      <a href="<?php echo home_url(); ?>/course07">
        <img src="<?php echo get_template_directory_uri();?>/img/course07.jpg" alt="">
        <h3>【ホワイトデー】<br>乾杯ロゼスパークリング付！<br>
          魚＆肉の Wメインと <Br>デザートなど全6品</h3>
      </a>
    </div>
    <div class="coursetitle pppp fadein">
      <a href="<?php echo home_url(); ?>/course08">
        <img src="<?php echo get_template_directory_uri();?>/img/course08.jpg" alt="">
        <h3>【本格フレンチをご堪能】<br>特製アペリティフ＆<br>厳選ワインの計2杯付き！<br>横浜ベイの夜景と全4品をご堪能</h3>
      </a>
    </div>

    <div class="menulink">
        <div class="l-menu">
            <a href="<?php echo home_url(); ?>/drink/"><< ドリンクメニュー</a>
        </div>
        <div class="r-menu">
            <a href="<?php echo home_url(); ?>/food/">フードメニュー >></a>
        </div>

    </div>


</div>

<?php get_footer(); ?>
