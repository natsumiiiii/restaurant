<?php get_header(); ?>

<div class="qa_page">
  <h2>Q&A</h2>
  <h3>〜よくあるご質問〜</h3>

  <div class="qa_midashi">
    <p class="qa_title">サービスについて</p>
        <p class="question">Q1. 記念日のサービスはありますか。</p>
          <p class="answer">アニバーサリーケーキ、花束、フラワーアレンジメントのご注文を承ります｡
            <br>・アニバーサリーケーキ　¥2,298〜（税・サービス料込）
            <br>・花束、フラワーアレンジメント　¥1,188～（税込）
            <br>※薔薇の花束については、こちらをご覧ください。
            <br>※クリスマス期間は上記メニューは除外となります。</p>
        <p class="question">Q2. プレゼントできる商品券やエステチケットはありますか？</p>
          <p class="answer">ランチとエステがセットになった女性向けチケット「ランチ＆ビューティー」があります。</p>
        <p class="question">Q3. ドレスコードはありますか？</p>
          <p class="answer">ラ・プロヴァンス、鉄板焼 匠、マンハッタンにつきましては、男性のお客様にはTシャツ、タンクトップ、ジーパン、短パン、サンダルなど肌の露出が多いお召し物は御遠慮いただいております。
            <br>その他のレストランでは、特にドレスコードはございません。</p>
  </div>

  <div class="qa_midashi">
        <p class="qa_title">料金について</p>
          <p class="question">Q1. レストランは、事前精算ですか？</p>
            <p class="answer">事前のお支払はございません。食事代金はご利用当日、各レストランにてお支払いください。</p>
          <p class="question">Q2. レストラン予約を取消した場合、取消料はかかりますか？</p>
            <p class="answer">取消料はいただいておりません。</p>
          <p class="question">Q3. お食事券や株主優待券と併用できるのですか？</p>
            <p class="answer">はい。お食事券や株主優待券と併用できます。</p>
          <p class="question">Q4. クレジットカードでの支払は可能ですか？</p>
            <p class="answer">はい。ご利用いただけます。
                <br>ご利用いただけますクレジットカードは『VISA、MASTER、DC、JCB、AMEX、DinersClub UCカード』となります。
                <br>なお、ギフトカードに関しましては『DC、JCB、UC』のみご利用いただけます。
                <br>『VJAギフトカード（旧VISAギフトカード）』はご利用いただけませんので、 予めご了承くださいませ。</p>
          <p class="question">Q5.キャッシュレス精算は何が使えますか？ </p>
            <p class="answer">フロント精算にてPayPayとAlipayがご利用いただけます。
                <br>また売店のみSuica（交通系電子マネー）がご利用いただけます。</p>
  </div>
  <div class="qa_midashi">
        <p class="qa_title">施設について</p>
        <p class="question">Q1. ＡＴＭはありますか？</p>
          <p class="answer">ございません。</p>
        <p class="question">Q2. 駐車場の利用は？</p>
          <p class="answer">お車でご来館のお客様には、ホテル地下の公共の駐車場をご利用いただいております。
            <br>レストランのご利用が1店舗につき¥5,000以上のお客様には、2時間分の駐車場料金をサービスさせていただきます。
            <br>全店対象（シェフズ ライブ キッチン、ラ・プロヴァンス、イタリアンダイニング ジリオン、鉄板焼 匠、ニューヨークラウンジ、マンハッタン）</p>
        <p class="question">Q3. レストランは分煙していますか？</p>
          <p class="answer">全レストラン＆ラウンジにおいて、全席禁煙です。</p>
        <p class="question">04. レストランは分煙していますか？</p>
          <p class="answer">全レストラン＆ラウンジにおいて、全席禁煙です。</p>
        <p class="question">Q5. バリアフリー対応ですか？</p>
          <p class="answer">すべてのレストランがバリアフリーとなっております。</p>
  </div>

</div>


<?php get_footer(); ?>
