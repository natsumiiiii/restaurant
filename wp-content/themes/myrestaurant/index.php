<?php
/*
Template Name:トップページ用テンプレート;
*/
?>

<?php get_header("2"); ?>
   <div id="contents">
      <div id="main">
          <img class="topimg" src="<?php echo get_template_directory_uri();?>/img/TOP_1.jpg" alt="">
      </div><!-- /#main -->
      <div class="top">
          <h2 class="fadein">いろんな味で豊かになる<span>、</span>料理も、人生も。</h2>
          <p class="fadein">こだわり抜いた素材、古くから語り継がれたこの味は
          <br>今でも多くの人に愛されている</p>
          <h4 class="fadein">来て　<br class="spbr">みて　<br class="spbr">感じて　<br class="spbr">味わって　</h4>
      </div>
      <div class="menuwrap">
        <img class="fadein" src="<?php echo get_template_directory_uri();?>/img/menu1.jpg" alt="">
        <div class="menu">
        <h3 class="fadein"><span>M</span>enu</h3>
            <ul>
              <li class="fadein box"><a href="<?php echo home_url(); ?>/course/">Cource</a></li>
              <li class="fadein box"><a href="<?php echo home_url(); ?>/food/">Food</a></li>
              <li class="fadein box"><a href="<?php echo home_url(); ?>/drink/">Drink</a></li>
            </ul>
        </div>
      </div>
      <div class="prowrap">
        <img  class="fadein" src="<?php echo get_template_directory_uri();?>/img/pro.jpg" alt="">
        <div class="pro">
            <h3 class="fadein"><span>P</span>rofessional</h3>
            <p class="fadein">当店の料理のプロをご紹介</p>
            <p class="fadein">最高のおもてなしを</p>
            <p class="fadein box member"><a href="<?php echo home_url(); ?>/good/">members >></a></p>
        </div>
      </div>
      <div class="infowrap">
        <img class="fadein" src="<?php echo get_template_directory_uri();?>/img/info.jpg" alt="">
        <div class="info">
        <h3 class="fadein"><span>I</span>nformation</h3>
            <ul>
              <li class="fadein box"><a href="<?php echo home_url(); ?>/Commitment/">Commitment</a></li>
              <li class="fadein box"><a href="<?php echo home_url(); ?>/appearance/">Appearance</a></li>
            </ul>
        </div>
      </div>
      <div class="contactwrap">
        <div class="contact">
        <h3 class="fadein"><span>C</span>ontact</h3>
        </div>
        <div class="contactlist fadein">
          <div class="con1">
            <a href="<?php echo home_url(); ?>/mail/">
                <p>お問い合せ</p>
                <img src="<?php echo get_template_directory_uri();?>/img/con1.jpg" alt="">
            </a>
          </div>
          <div class="con2">
            <a href="<?php echo home_url(); ?>/reservation/">
                <p>ご予約はこちら</p>
                <img src="<?php echo get_template_directory_uri();?>/img/con2.jpg" alt="">
            </a>
          </div>
          <div class="con3">
            <a href="<?php echo home_url(); ?>/questions/">
              <p>よくあるご質問</p>
              <img src="<?php echo get_template_directory_uri();?>/img/con3.jpg" alt="">
            </a>
          </div>
        </div>
      </div>
      <div class="photwrap">
        <div class="phot fadein">
          <h3><span>P</span>hoto <span>G</span>allery</h3>
        </div>
        <div id="photgallery" class="fadein">
            <ul class="slider">
              <li><img src="<?php echo get_template_directory_uri();?>/img/phot1.jpg" alt="phot1"></li>
              <li><img src="<?php echo get_template_directory_uri();?>/img/phot2.jpg" alt="phot2"></li>
              <li><img src="<?php echo get_template_directory_uri();?>/img/phot3.jpg" alt="phot3"></li>
              <li><img src="<?php echo get_template_directory_uri();?>/img/phot4.jpg" alt="phot4"></li>
              <li><img src="<?php echo get_template_directory_uri();?>/img/phot5.jpg" alt="phot5"></li>
              <li><img src="<?php echo get_template_directory_uri();?>/img/phot6.jpg" alt="phot6"></li>
              <li><img src="<?php echo get_template_directory_uri();?>/img/phot7.jpg" alt="phot7"></li>
              <li><img src="<?php echo get_template_directory_uri();?>/img/phot8.jpg" alt="phot8"></li>
           </ul>
        </div>
        <div class="photlink fadein">
            <a  href="<?php echo home_url(); ?>/photogallery/">~ more ~</a>
        </div>
      </div>
   </div><!-- /#contents -->

<?php get_footer(); ?>
