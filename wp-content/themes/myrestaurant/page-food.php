<?php get_header(); ?>

<div class="foodmenu">

<h2>フードメニュー</h2>

<div class="foodwrap">
  <div class="foods">
      <img src="<?php echo get_template_directory_uri();?>/img/food01.jpg" alt="">
      <h4>タイトル</h4>
  </div>
  <div class="foods">
      <img src="<?php echo get_template_directory_uri();?>/img/food02.jpg" alt="">
      <h4>タイトル</h4>
  </div>
  <div class="foods">
      <img src="<?php echo get_template_directory_uri();?>/img/food03.jpg" alt="">
      <h4>タイトル</h4>
  </div>
  <div class="foods">
      <img src="<?php echo get_template_directory_uri();?>/img/food04.jpg" alt="">
      <h4>タイトル</h4>
  </div>
  <div class="foods">
      <img src="<?php echo get_template_directory_uri();?>/img/food05.jpg" alt="">
      <h4>タイトル</h4>
  </div>
  <div class="foods">
      <img src="<?php echo get_template_directory_uri();?>/img/food06.jpg" alt="">
      <h4>タイトル</h4>
  </div>
  <div class="foods">
      <img src="<?php echo get_template_directory_uri();?>/img/food07.jpg" alt="">
      <h4>タイトル</h4>
  </div>
  <div class="foods">
      <img src="<?php echo get_template_directory_uri();?>/img/food08.jpg" alt="">
      <h4>タイトル</h4>
  </div>
  <div class="foods">
      <img src="<?php echo get_template_directory_uri();?>/img/food09.jpg" alt="">
      <h4>タイトル</h4>
  </div>
  <div class="foods">
      <img src="<?php echo get_template_directory_uri();?>/img/food10.jpg" alt="">
      <h4>タイトル</h4>
  </div>
</div>

<div class="menulink">
    <div class="l-menu">
        <a href="<?php echo home_url(); ?>/course/"><<　コース料理 一覧</a>
    </div>
    <div class="r-menu">
        <a href="<?php echo home_url(); ?>/drink/">ドリンクメニュー　>></a>
    </div>

</div>

</div>


<?php get_footer(); ?>
