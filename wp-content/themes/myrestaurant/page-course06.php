<?php get_header(); ?>

<div class="coursepage">

  <h2>【シェフセレクション】<br>フレッシュフォアグラのソテーを楽しむ<br>和牛メインのコース料理！<br>シェフお薦全6品</h2>
  <h3>〜メニュー〜</h3>
  <p>＜メニュー例＞&nbsp;</p>
  <p>■アミューズ&nbsp;</p>
  <p>■前菜&nbsp;</p>
  <p>■フレッシュフォアグラのソテー&nbsp;</p>
  <p>■魚料理&nbsp;</p>
  <p>■牛肉料理 （和牛）&nbsp;</p>
  <p>■デザート&nbsp;</p>
  <p>■コーヒー</p>
  <br>
  <p>※メニューは仕入状況等により変更となる場合がございます。予めご了承ください。</p>

</div>

<div class="backimg">
  <img src="<?php echo get_template_directory_uri();?>/img/course01.jpg" alt="">
</div>

<?php get_footer(); ?>
