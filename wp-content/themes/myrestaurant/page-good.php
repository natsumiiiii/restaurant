<?php get_header(); ?>

<div class="goodpage">

  <h2>料理のプロ</h2>

  <div class="chef fadein">
     <img src="<?php echo get_template_directory_uri();?>/img/chef01.jpg" alt="">
     <div class="chef01">
       <h4>Top chef</h4>
       <h3>Taro Moriyama</h3>
       <p>1994.10.01生まれ　○○出身</p>
       <p>○○大学卒業後、...△△。</p>
       <p>//////////////////</p>
       <p>..................</p>
     </div>
  </div>

  <div class="chef chef2 fadein">
     <img src="<?php echo get_template_directory_uri();?>/img/chef02.jpg" alt="">
     <div class="chef02">
       <h4>Patissier</h4>
       <h3>Hanako Moriyama</h3>
       <p>1999.12.31生まれ　○○出身</p>
       <p>○○大学卒業後、...△△。</p>
       <p>//////////////////</p>
       <p>..................</p>
     </div>
  </div>


</div>


<?php get_footer(); ?>
