<?php

//ウィジェットエリアの追加
register_sidebar(
  array(
      'name' => '私のウィジェットエリア',
      'id' => 'my-sidebar'
      )
  );

//カスタムメニューの登録
register_nav_menu('footer_menu','フッターメニュー');

//タイトルタグの設定
function my_setup_theme() {
    add_theme_support('title-tag');
}
add_action('after_setup_theme','my_setup_theme');
