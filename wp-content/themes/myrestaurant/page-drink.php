<?php get_header(); ?>

<div class="drinkmenu">

<h2>ドリンクメニュー</h2>

    <div class="drinktitle">
      <h3>Soft Drink</h3>
        <ul>
          <li>ジンジャー　エール<span>￥550</span></li>
          <li>コーラ<span>￥550</span></li>
          <li>フレッシュ　オレンジ<span>￥1,320</span></li>
          <li>フレッシュ　グレープフルーツ<span>￥1,320</span></li>
          <li>レモン　スカッシュ<span>￥1,320</span></li>
          <li>トマト　ジュース<span>￥660</span></li>
          <li>ペリエ<span>￥880</span></li>
          <li>ウーロン茶<span>￥550</span></li>
        </ul>
    </div>
    <div class="drinktitle">
      <h3>Glass Wine</h3>
        <ul>
          <li>グラスワイン（白or赤）<span>￥1,100</span></li>
          <li>グラスシャンパン<span>￥1,650</span></li>
        </ul>
    </div>
    <div class="drinktitle">
      <h3>Other Drinks</h3>
      <ul><li>キール<span>￥1,100</span></li>
        <li>キールロワイヤル<span>￥1,320</span></li>
        <li>ミモザ<span>￥1,320</span></li>
        <li>ジン トニック<span>￥1,320</span></li>
      </ul>
    </div>
    <div class="drinktitle">
      <h3>Japanese Whisky</h3>
      <ul>
        <li>ニッカ鶴　１７年<span>￥2,750</span></li>
        <li>竹鶴 ２１年<span>￥1,650</span></li>
        <li>宮城峡 １２年<span>￥1,430</span></li>
        <li>ローヤル<span>¥2,000</span></li>
      </ul>
    </div>

    <div class="menulink">
        <div class="l-menu">
            <a href="<?php echo home_url(); ?>/food/"><< フードメニュー</a>
        </div>
        <div class="r-menu">
            <a href="<?php echo home_url(); ?>/course/">コース料理 一覧 >></a>
        </div>

    </div>


</div>


<?php get_footer(); ?>
